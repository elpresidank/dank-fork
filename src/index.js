'use strict';
import Favicon  from './assets/img/favicon.png';
import Icons from './assets/img/icons.svg';
import Logo from './assets/img/logo.png';

const recipeContainer = document.querySelector('.recipe');

const timeout = function (s) {
  return new Promise(function (_, reject) {
    setTimeout(function () {
      reject(new Error(`Request took too long! Timeout after ${s} second`));
    }, s * 1000);
  });
};

// https://forkify-api.herokuapp.com/v2

///////////////////////////////////////